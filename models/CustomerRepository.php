<?php
include "IRepository.php";
include "Customer.php";

class CustomerRepository implements IRepository {

    protected $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    function read($row) {
        $result = new Customer();
        $result->customerNumber = $row["customerNumber"];
        $result->customerName = $row["customerName"];
        $result->contactLastName = $row["contactLastName"];
        $result->contactFirstName = $row["contactFirstName"];
        $result->phone = $row["phone"];
        $result->addressLine1 = $row["addressLine1"];
        $result->addressLine2 = $row["addressLine2"];
        $result->city = $row["city"];
        $result->state = $row["state"];
        $result->country = $row["country"];
        $result->postalCode = $row["postalCode"];
        $result->salesRepEmployeeNumber = $row["salesRepEmployeeNumber"];
        $result->creditLimit = $row["creditLimit"];
        return $result;
    }

    public function getAll($filter) {
        $sql = "SELECT * FROM customers";
        $q = $this->db->prepare($sql);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        $idx = 0;
        foreach($rows as $row) {
            $idx += 1;
            if ($idx < 10)
            {array_push($result, $this->read($row));}
        }
        return $result;
    }

}
