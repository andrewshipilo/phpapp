<?php
include "IRepository.php";
include "Order.php";

class OrderRepository implements IPostRepository, IPutRepository, IRemoveRepository {

    protected $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    function read($row) {
        $result = new Order();
        $result->orderNumber = $row['orderNumber'];
        $result->orderDate = $row['orderDate'];
        $result->requiredDate = $row['requiredDate'];
        $result->shippedDate = $row['shippedDate'];
        $result->status = $row['status'];
        $result->comments = $row['comments'];
        $result->customerNumber = $row['customerNumber'];

        return $result;
    }

    public function getById($emp_no) {
        $sql = "SELECT * FROM orders WHERE orderNumber = :orderNumber";
        $q = $this->db->prepare($sql);
        $q->bindParam(":orderNumber", $orderNumber, PDO::PARAM_INT);
        $q->execute();
        $rows = $q->fetchAll();
        return $this->read($rows[0]);
    }

    public function getAll($filter) {
        $orderDate = "%" . $filter["orderDate"] . "%";
        $requiredDate = "%" . $filter["requiredDate"] . "%";
        $shippedDate = "%" . $filter["shippedDate"] . "%";
        $status = "%" . $filter["status"] . "%";
        $comments = "%" . $filter["comments"] . "%";
        $customerNumber = "%" . $filter["customerNumber"] . "%";

        $sql = "SELECT * FROM orders WHERE orderDate LIKE :orderDate AND requiredDate LIKE :requiredDate AND 
                              shippedDate LIKE :shippedDate AND status LIKE :status AND comments LIKE :comments AND 
                              customerNumber LIKE :customerNumber ORDER BY orderNumber ASC";

        $q = $this->db->prepare($sql);
        $q->bindParam(":orderDate", $orderDate);
        $q->bindParam(":requiredDate", $requiredDate);
        $q->bindParam(":shippedDate", $shippedDate);
        $q->bindParam(":status", $status);
        $q->bindParam(":comments", $comments);
        $q->bindParam(":customerNumber", $customerNumber);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }

    public function insert(array $data)
    {
        $max_sql = "SELECT MAX(orderNumber) FROM orders";
        $max_q = $this->db->prepare($max_sql);
        $max_q->execute();
        $rows = $max_q->fetchAll();

        $max = $rows[0][0] + 1;

        $sql = "INSERT INTO orders(orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber)
                            VALUES (:orderNumber, :orderDate, :requiredDate, :shippedDate, :status, :comments, :customerNumber)";

        $q = $this->db->prepare($sql);
        $q->bindParam(":orderNumber", $max);
        $q->bindParam(":orderDate", $data["orderDate"]);
        $q->bindParam(":requiredDate", $data["requiredDate"]);
        $q->bindParam(":shippedDate", $data["shippedDate"]);
        $q->bindParam(":status", $data["status"]);
        $q->bindParam(":comments", $data["comments"]);
        $q->bindParam(":customerNumber", $data["customerNumber"]);
        $q->execute();
        return $this->getById($this->db->lastInsertId());
    }

    public function update($data) {
        $sql = "UPDATE orders SET orderDate = :orderDate, requiredDate = :requiredDate, shippedDate = :shippedDate,
                     status = :status, comments = :comments, customerNumber = :customerNumber WHERE orderNumber = :orderNumber";

        $q = $this->db->prepare($sql);
        $q->bindParam(":orderDate", $data["orderDate"]);
        $q->bindParam(":requiredDate", $data["requiredDate"]);
        $q->bindParam(":shippedDate", $data["shippedDate"]);
        $q->bindParam(":status", $data["status"]);
        $q->bindParam(":comments", $data["comments"]);
        $q->bindParam(":customerNumber", $data["customerNumber"]);
        $q->bindParam(":orderNumber", $data["orderNumber"], PDO::PARAM_INT);
        $q->execute();

        return $data;
    }

    public function remove($emp_no) {
        $sql = "DELETE FROM employees WHERE orderNumber = :orderNumber";
        $q = $this->db->prepare($sql);
        $q->bindParam(":orderNumber", $emp_no, PDO::PARAM_INT);
        $q->execute();
        return $emp_no;
    }
}