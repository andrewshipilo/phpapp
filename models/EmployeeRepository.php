<?php
include "IRepository.php";
include "Employee.php";
require_once "../functions/query.php";

class EmployeeRepository implements IPostRepository, IPutRepository, IRemoveRepository {
    
    protected $db;
    
    public function __construct(PDO $db) {
        $this->db = $db;
    }
    
    function read($row) {
        $result = new Employee();
        $result->employeeNumber = $row['employeeNumber'];
        $result->lastName = $row['lastName'];
        $result->firstName = $row['firstName'];
        $result->extension = $row['extension'];
        $result->email = $row['email'];
        $result->officeCode = $row['officeCode'];
        $result->reportsTo = $row['reportsTo'];
        $result->jobTitle = $row['jobTitle'];
        return $result;
    }
    
    public function getById($emp_no) {
        $sql = "SELECT * FROM employees WHERE employeeNumber = :employeeNumber";
        $q = $this->db->prepare($sql);
        $q->bindParam(":employeeNumber", $emp_no, PDO::PARAM_INT);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }
    public function addToSql(&$sql, $filter, $variable)
    {
        if ($filter[$variable] != '')
        {
            $sql = $sql . ' AND ' . $variable . ' like :' . $variable;
        }
    }

    public function bindParam(&$q, $param, $filter)
    {
        if ($filter[$param] != '')
        {
            $var = '%' . $filter[$param] . '%';
            $q->bindParam(':'.$param, $var, PDO::PARAM_STR);
        }
    }

    public function getAll($filter) {
        $sql = "SELECT * FROM employees WHERE 1";

        $this->addToSql($sql, $filter, 'lastName');
        $this->addToSql($sql, $filter, 'firstName');
        $this->addToSql($sql, $filter, 'extension');
        $this->addToSql($sql, $filter, 'email');
        $this->addToSql($sql, $filter, 'officeCode');
        $this->addToSql($sql, $filter, 'reportsTo');
        $this->addToSql($sql, $filter, 'jobTitle');
        $sql = $sql . " ORDER BY employeeNumber";
        /*
        $lastName = "%" . $filter["lastName"] . "%";
        $firstName = "%" . $filter["firstName"] . "%";
        $extension = "%" . $filter["extension"] . "%";
        $email = "%" . $filter["email"] . "%";
        $officeCode = "%" . $filter["officeCode"] . "%";
        $reportsTo = "%" . $filter["reportsTo"] . "%";
        $jobTitle = "%" . $filter["jobTitle"] . "%";
        */

        $q = $this->db->prepare($sql);
        $this->bindParam($q, 'lastName', $filter);
        $this->bindParam($q, 'firstName', $filter);
        $this->bindParam($q, 'extension', $filter);
        $this->bindParam($q, 'email', $filter);
        $this->bindParam($q, 'officeCode', $filter);
        $this->bindParam($q, 'reportsTo', $filter);
        $this->bindParam($q, 'jobTitle', $filter);

        /*if ($lastName != '') $q->bindParam(":lastName", $lastName, PDO::PARAM_STR);
        if ($firstName != '') $q->bindParam(":firstName", $firstName, PDO::PARAM_STR);
        if ($extension != '') $q->bindParam(":extension", $extension, PDO::PARAM_STR);
        if ($email != '') $q->bindParam(":email", $email, PDO::PARAM_STR);
        if ($officeCode != '') $q->bindParam(":officeCode", $officeCode, PDO::PARAM_STR);
        if ($reportsTo != '') $q->bindParam(":reportsTo", $reportsTo, PDO::PARAM_STR);
        if ($jobTitle != '') $q->bindParam(":jobTitle", $jobTitle, PDO::PARAM_STR);
        */
        $q->execute();
        #$q->debugDumpParams();

        print_debug(interpolateQuery($sql, $filter));


        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }

    public function insert(array $data)
    {
        $max_sql = "SELECT MAX(employeeNumber) FROM employees";
        $max_q = $this->db->prepare($max_sql);
        $max_q->execute();
        $rows = $max_q->fetchAll();

        $max = $rows[0][0] + 1;

        $sql = "INSERT INTO employees(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle)
                            VALUES (:employeeNumber, :lastName, :firstName, :extension, :email, :officeCode, :reportsTo, :jobTitle)";

        $q = $this->db->prepare($sql);
        $q->bindParam(":employeeNumber", $max);
        $q->bindParam(":lastName", $data["lastName"]);
        $q->bindParam(":firstName", $data["firstName"]);
        $q->bindParam(":extension", $data["extension"]);
        $q->bindParam(":email", $data["email"]);
        $q->bindParam(":officeCode", $data["officeCode"]);
        $q->bindParam(":reportsTo", $data["reportsTo"]);
        $q->bindParam(":jobTitle", $data["jobTitle"]);
        $q->execute();
        #print_debug($q->queryString);
        #print_debug("New employee inserted " . $max . $data["lastName"] . $data["firstName"]);
        print_debug(interpolateQuery($sql, $data));
        return $this->getById($max);
    }

    public function update($data) {
        $sql = "UPDATE employees SET lastName = :lastName, firstName = :firstName, extension = :extension,
                     email = :email, officeCode = :officeCode, reportsTo = :reportsTo, jobTitle = :jobTitle WHERE employeeNumber = :employeeNumber";

        $q = $this->db->prepare($sql);
        $q->bindParam(":lastName", $data["lastName"]);
        $q->bindParam(":firstName", $data["firstName"]);
        $q->bindParam(":extension", $data["extension"]);
        $q->bindParam(":email", $data["email"]);
        $q->bindParam(":officeCode", $data["officeCode"]);
        $q->bindParam(":reportsTo", $data["reportsTo"]);
        $q->bindParam(":jobTitle", $data["jobTitle"]);
        $q->bindParam(":employeeNumber", $data["employeeNumber"], PDO::PARAM_INT);
        $q->execute();

        return $data;
    }

    public function remove($emp_no) {
        $sql = "DELETE FROM employees WHERE employeeNumber = :employeeNumber";
        $q = $this->db->prepare($sql);
        $q->bindParam(":employeeNumber", $emp_no, PDO::PARAM_INT);
        $q->execute();
        return $emp_no;
    }
}