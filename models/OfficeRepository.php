<?php
include "IRepository.php";
include "Office.php";

class OfficeRepository implements IRepository {

    protected $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    function read($row) {
        $result = new Office();
        $result->officeCode = $row["officeCode"];
        $result->city = $row["city"];
        $result->phone = $row["phone"];
        $result->addressLine1 = $row["addressLine1"];
        $result->addressLine2 = $row["addressLine2"];
        $result->state = $row["state"];
        $result->country = $row["country"];
        $result->postalCode = $row["postalCode"];
        $result->territory = $row["territory"];
        return $result;
    }

    public function getAll($filter) {
        $sql = "SELECT * FROM offices";
        $q = $this->db->prepare($sql);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }

}
