<?php
include "IRepository.php";
include "User.php";

class UserRepository implements IPostRepository, IPutRepository, IRemoveRepository {

    protected $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    function read($row) {
        $result = new User();
        $result->user_id = $row['user_id'];
        $result->login = $row['login'];
        $result->password = $row['password'];
        $result->access = $row['access'];
        $result->banned = $row['banned'];
        $result->session_id = $row['session_id'];
        return $result;
    }

    public function getById($user_id) {
        $sql = "SELECT * FROM users WHERE user_id = :user_id";
        $q = $this->db->prepare($sql);
        $q->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }

    public function getAll($filter) {
        $login = "%" . $filter["login"] . "%";
        $password = "%" . $filter["password"] . "%";
        $access = "%" . $filter["access"] . "%";
        $banned = "%" . $filter["banned"] . "%";
        $session_id = "%" . $filter["session_id"] . "%";

        $sql = "SELECT * FROM users WHERE login LIKE :login AND password LIKE :password AND 
                              access LIKE :access AND banned LIKE :banned AND session_id LIKE :session_id ORDER BY user_id ASC";

        $q = $this->db->prepare($sql);
        $q->bindParam(":login", $login);
        $q->bindParam(":password", $password);
        $q->bindParam(":access", $access);
        $q->bindParam(":banned", $banned);
        $q->bindParam(":session_id", $session_id);

        $q->execute();

        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }

    public function insert(array $data)
    {
        $max_sql = "SELECT MAX(user_id) FROM users";
        $max_q = $this->db->prepare($max_sql);
        $max_q->execute();
        $rows = $max_q->fetchAll();

        $max = $rows[0][0] + 1;

        $sql = "INSERT INTO users(user_id, login, password, access, banned, session_id)
                            VALUES (:user_id, :login, :password, :access, :banned, :session_id)";

        $q = $this->db->prepare($sql);
        $password = hash("md5", $data["login"] . $data["password"] . 'salt');
        $q->bindParam(":user_id", $max);
        $q->bindParam(":login", $data["login"]);
        $q->bindParam(":password", $password);
        $q->bindParam(":access", $data["access"]);
        $q->bindParam(":banned", $data["banned"]);
        $q->bindParam(":session_id", $data["session_id"]);
        $q->execute();

        print_debug("New user inserted " . $max . $data["login"] . $password);

        return $this->getById($max);
    }

    public function update($data) {
        $sql = "UPDATE users SET login = :login, password = :password, access = :access,
                     banned = :banned, session_id = :session_id WHERE user_id = :user_id";

        $q = $this->db->prepare($sql);
        $data["password"] = hash("md5", $data["login"] . $data["password"] . 'salt');
        $q->bindParam(":login", $data["login"]);
        $q->bindParam(":password", $data["password"]);
        $q->bindParam(":access", $data["access"]);
        $q->bindParam(":banned", $data["banned"]);
        $q->bindParam(":session_id", $data["session_id"]);
        $q->bindParam(":user_id", $data["user_id"], PDO::PARAM_INT);
        $q->execute();

        return $data;
    }

    public function remove($user_id) {
        $sql = "DELETE FROM users WHERE user_id = :user_id";
        $q = $this->db->prepare($sql);
        $q->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $q->execute();
        return $user_id;
    }
}