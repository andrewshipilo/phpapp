<?php

interface IRepository
{
    function read($row);
    public function getAll($filter);
}

interface IPostRepository extends IRepository
{
    public function insert(array $data);
}

interface IPutRepository extends IRepository
{
    public function update($data);
}

interface IRemoveRepository extends IRepository
{
    public function remove($id);
}