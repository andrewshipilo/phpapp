<?php
class Customer {
    public $customerNumber;
    public $customerName;
    public $contactLastName;
    public $contactFirstName;
    public $phone;
    public $addressLine1;
    public $addressLine2;
    public $city;
    public $state;
    public $postalCode;
    public $country;
    public $salesRepEmployeeNumber;
    public $creditLimit;
}