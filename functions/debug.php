<?php
require_once "../config/config.php";

function print_debug($message)
{
    if (defined('DEBUG'))
    {
        if (DEBUG === true)
        {
            if ($message == '')
            {
                parse_str(file_get_contents("php://input"), $_PUT);
                error_log('Get and post:' . "\n\t" . 'GET: ' . json_encode($_GET)
                    . "\n\t" . "POST: " . json_encode($_POST) . "\n\t" . "PUT/DELETE: " . json_encode($_PUT). "\n", 3, DEBUGLOG);
            }
            else
            {
                error_log('Debug message:' . htmlentities($message) . "\n", 3, DEBUGLOG);
            }
        }
    }
}