<?php
require_once __DIR__.'/../vendor/autoload.php';

function template_create($path, $vars) {
    global $url_base, $url_home;
    extract($vars);

    //Init twig
    $loader = new Twig_Loader_Filesystem(__DIR__.'/../templates');
    $twig = new Twig_Environment($loader, array('debug' => true));


    foreach ($vars as &$var)
    {
        $var = htmlspecialchars($var);
    }
    echo $twig->render(trim($path, '/') . '.twig', $vars);
    exit;
}