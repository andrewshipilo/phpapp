<?php
require_once "debug.php";

// Connect to DB
function connect($db, $username, $password)
{
    try
    {
        $db = new PDO($db, $username, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }
    catch (PDOException $PDOException)
    {
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../templates');
        $twig = new Twig_Environment($loader, array('debug' => DEBUG));

        print_debug('Problem with database, see current env logs to know more');
        $logfile = '';
        if (DEBUG)
        {
            $logfile = DEBUGLOG;
        }
        else
        {
            $logfile = PRODLOG;
        }

        error_log(date("Y-m-d H:i:s") . $PDOException->getMessage(). "\n", 3, $logfile);
        header("Location: /");
        template_create('error', array(
            'title' => 'DBError'
        ));
        die();
    }
    catch (Exception $exception)
    {
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../templates');
        $twig = new Twig_Environment($loader, array('debug' => DEBUG));

        error_log(date("Y-m-d H:i:s") . htmlentities($exception->getMessage()). "\n", 3, LOGFILE);
        template_create('error', array(
            'title' => 'Error'
        ));
        die();
    }
}
