<?php

$routes = array();

function route_add($patterns, $callback) {
    global $routes;
    // Be sure that patterns is array
    if( ! is_array($patterns)) {
        $patterns = array($patterns);
    }
    // Check every given pattern
    foreach($patterns as $pattern) {
        // Delete useless first and last /
        $pattern = trim($pattern, '/');
        // Replace given combinations to corresponding regular expressions
        $pattern = str_replace(
            array(
                '\(',
                '\)',
                '\|',
                '\:any',
                '\:num',
                '\:all',
                '#'
            ),
            array(
                '(',
                ')',
                '|',
                '[^/]+',
                '\d+',
                '.*?',
                '\#'
            ),
            preg_quote($pattern, '/'));
        // Add provided callback to each route
        $routes['#^' . $pattern . '$#'] = $callback;
    }
}

function route_execute() {
    global $routes;
    $url = $_SERVER['REQUEST_URI'];
    $base = str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME']));
    // If url was found in base
    if (strpos($url, $base) === 0) {
        // Shorten url
        $url = substr($url, strlen($base));
    }
    $url = trim($url, '/');
    foreach($routes as $pattern => $callback) {
        // Check if our url matches any of routes using regular expression match
        if (preg_match($pattern, $url, $params)) {
            array_shift($params);
            // Run the callback (usually just loads route page)
            return call_user_func_array($callback, array_values($params));
        }
    }
}
