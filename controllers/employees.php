<?php

include "../models/EmployeeRepository.php";
#include "../config/config.php";
include "../functions/sanitize.php";
include "../functions/db.php";


$db = connect("mysql:host=localhost;dbname=classicmodels", $_SESSION["login"], $_SESSION["password"]);
$employees = new EmployeeRepository($db);

header("Content-Type: application/json");

try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $lastName = sanitize($_GET["lastName"], PARANOID);
            $firstName = sanitize($_GET["firstName"], PARANOID);
            $extension = sanitize($_GET["extension"], PARANOID);

            $email = $_GET["email"];
            if ($email != '') {
                $email = sanitize_email($_GET["email"]);
            }

            $officeCode = $_GET["officeCode"];
            if ($officeCode != '') {
                if (check_int($_GET["officeCode"]))
                    $officeCode = sanitize_int($_GET["officeCode"]);
            }

            $reportsTo = $_GET["reportsTo"];
            if ($reportsTo != '') {
                if (check_int($_GET["reportsTo"], 1000, 4000))
                    $reportsTo = sanitize_int($_GET["reportsTo"]);
            }

            $jobTitle = sanitize($_GET["jobTitle"], PARANOID);

            $result = $employees->getAll(array(
                "lastName" => $lastName,
                "firstName" => $firstName,
                "extension" => $extension,
                "email" => $email,
                "officeCode" => $officeCode,
                "reportsTo" => $reportsTo,
                "jobTitle" => $jobTitle
            ));
            break;

        case "POST":
            $lastName = sanitize($_POST["lastName"], PARANOID);
            $firstName = sanitize($_POST["firstName"], PARANOID);
            $extension = sanitize($_POST["extension"], PARANOID);

            $email = $_POST["email"];
            if ($email != '') {
                $email = sanitize_email($_POST["email"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Email is empty" . "\n", 3, LOGFILE);
                throw new Exception("Email is empty",230);
            }

            $officeCode = $_POST["officeCode"];
            if ($officeCode != '') {
                if (check_int($_POST["officeCode"]))
                    $officeCode = sanitize_int($_POST["officeCode"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Office code is empty" . "\n", 3, LOGFILE);
                throw new Exception("Office code is empty",230);
            }

            $reportsTo = $_POST["reportsTo"];
            if ($reportsTo != '') {
                if (check_int($_POST["reportsTo"], 1000, 4000))
                    $reportsTo = sanitize_int($_POST["reportsTo"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "ReportsTo is empty" . "\n", 3, LOGFILE);
                throw new Exception("ReportsTo is empty",230);
            }

            $jobTitle = sanitize($_POST["jobTitle"], PARANOID);

            $result = $employees->insert(array(
                "lastName" => $lastName,
                "firstName" => $firstName,
                "extension" => $extension,
                "email" => $email,
                "officeCode" => $officeCode,
                "reportsTo" => $reportsTo,
                "jobTitle" => $jobTitle
            ));
            break;

        case "PUT":
            parse_str(file_get_contents("php://input"), $_PUT);
            $employeeNumber = $_PUT["employeeNumber"];
            if ($employeeNumber != '') {
                if (check_int($_PUT["employeeNumber"]))
                    $employeeNumber = sanitize_int($_PUT["employeeNumber"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "EmployeeNum is empty" . "\n", 3, LOGFILE);
                $db = null;
                throw new Exception("EmployeeNum is empty",230);
            }

            $lastName = sanitize($_PUT["lastName"], PARANOID);
            $firstName = sanitize($_PUT["firstName"], PARANOID);
            $extension = sanitize($_PUT["extension"], PARANOID);

            $email = $_PUT["email"];
            if ($email != '') {
                $email = sanitize_email($_PUT["email"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Email is empty" . "\n", 3, LOGFILE);
            }

            $officeCode = $_PUT["officeCode"];
            if ($officeCode != '') {
                if (check_int($_PUT["officeCode"]))
                    $officeCode = sanitize_int($_PUT["officeCode"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Office code is empty" . "\n", 3, LOGFILE);
                throw new Exception("Office code is empty",230);
            }

            $reportsTo = $_PUT["reportsTo"];
            if ($reportsTo != '') {
                if (check_int($_PUT["reportsTo"], 1000, 4000))
                    $reportsTo = sanitize_int($_PUT["reportsTo"]);
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "ReportTo is empty" . "\n", 3, LOGFILE);
                throw new Exception("ReportTo is empty",230);
            }

            $jobTitle = sanitize($_PUT["jobTitle"], PARANOID);

            $result = $employees->update(array(
                "employeeNumber" => intval($employeeNumber),
                "lastName" => $lastName,
                "firstName" => $firstName,
                "extension" => $extension,
                "email" => $email,
                "officeCode" => $officeCode,
                "reportsTo" => $reportsTo,
                "jobTitle" => $jobTitle
            ));
            break;

        case "DELETE":
            parse_str(file_get_contents("php://input"), $_DELETE);
            $employeeNumber = $_DELETE["employeeNumber"];
            if ($employeeNumber != '') {
                if (check_int($_DELETE["employeeNumber"]))
                    $employeeNumber = sanitize_int($_DELETE["employeeNumber"]);
                else {
                    error_log(date("Y-m-d H:i:s") . ":" . "Employee number is not an int " . $employeeNumber . "\n", 3, LOGFILE);
                }
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Employee number is empty" . "\n", 3, LOGFILE);
                throw new Exception("Employee number is empty",230);
            }

            $result = $employees->remove(intval($employeeNumber));
            break;
    }

    echo json_encode($result);
} catch (Exception $exception)
{
    echo json_encode(array(
        'error' => array(
            'msg' => $exception->getMessage(),
            'code' => $exception->getCode(),
        ),
    ));
}

