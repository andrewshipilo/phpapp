<?php

include "../models/CustomerRepository.php";
include ("../config/config.php");
include "../functions/db.php";


$db = connect("mysql:host=localhost;dbname=classicmodels", "manager", "manager");
$customers = new CustomerRepository($db);

try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $result = $customers->getAll(array());
            break;
    }
    header("Content-Type: application/json");
    echo json_encode($result);
}
catch (Exception $exception)
{
    echo json_encode(array(
        'error' => array(
            'msg' => $exception->getMessage(),
            'code' => $exception->getCode(),
        ),
    ));
}


