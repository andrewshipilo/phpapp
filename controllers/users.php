<?php

include "../models/UserRepository.php";
include "../functions/sanitize.php";
include "../functions/db.php";

header("Content-Type: application/json");

$db = connect("mysql:host=localhost;dbname=classicmodels", $_SESSION["login"], $_SESSION["password"]);
$users = new UserRepository($db);


try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $login = sanitize($_GET["login"], PARANOID);
            $password = sanitize($_GET["password"], PARANOID);
            $access = sanitize($_GET["access"], PARANOID);
            $banned = sanitize($_GET["banned"], PARANOID);
            $session_id = $_GET["session_id"];

            $result = $users->getAll(array(
                "login" => $login,
                "password" => $password,
                "access" => $access,
                "banned" => $banned,
                "session_id" => $session_id
            ));
            break;

        case "POST":
            $login = sanitize($_POST["login"], PARANOID);
            $password = sanitize($_POST["password"], PARANOID);
            $access = sanitize($_POST["access"], PARANOID);
            $banned = sanitize($_POST["banned"], PARANOID);
            $session_id = $_GET["session_id"];

            $result = $users->insert(array(
                "login" => $login,
                "password" => $password,
                "access" => $access,
                "banned" => $banned,
                "session_id" => $session_id
            ));
            break;

        case "PUT":
            parse_str(file_get_contents("php://input"), $_PUT);
            print_debug('');
            $user_id = $_PUT["user_id"];

            if ($user_id != '') {
                if (check_int($_PUT["user_id"]))
                    $user_id = sanitize_int($_PUT["user_id"]);
                else {
                    error_log(date("Y-m-d H:i:s") . ":" . "Employee number is not an int " . $user_id . "\n", 3, LOGFILE);
                }
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Employee number is empty" . "\n", 3, LOGFILE);
                throw new Exception("Employee number is empty",230);
            }

            $login = sanitize($_PUT["login"], PARANOID);
            $password = sanitize($_PUT["password"], PARANOID);
            $access = sanitize($_PUT["access"], PARANOID);
            $banned = sanitize($_PUT["banned"], PARANOID);
            $session_id = $_GET["session_id"];

            $result = $users->update(array(
                "login" => $login,
                "password" => $password,
                "access" => $access,
                "banned" => $banned,
                "session_id" => $session_id,
                "user_id" => $user_id
            ));

            break;

        case "DELETE":
            parse_str(file_get_contents("php://input"), $_DELETE);
            $user_id = $_DELETE["user_id"];

            if ($user_id != '') {
                if (check_int($_DELETE["user_id"]))
                    $user_id = sanitize_int($_DELETE["user_id"]);
                else {
                    error_log(date("Y-m-d H:i:s") . ":" . "Employee number is not an int " . $user_id . "\n", 3, LOGFILE);
                }
            } else {
                error_log(date("Y-m-d H:i:s") . ":" . "Employee number is empty" . "\n", 3, LOGFILE);
                throw new Exception("Employee number is empty",230);
            }

            $result = $users->remove(intval($user_id));
            break;
    }

    echo json_encode($result);
} catch (Exception $exception)
{
    echo json_encode(array(
        'error' => array(
            'msg' => $exception->getMessage(),
            'code' => $exception->getCode(),
        ),
    ));
}

