<?php

include "../models/OrderRepository.php";
include ("../config/config.php");
include "../functions/sanitize.php";
include "../functions/db.php";
$db = connect("mysql:host=localhost;dbname=classicmodels", $_SESSION["login"], $_SESSION["password"]);
$orders = new OrderRepository($db);

const STATUS_WHITELIST = ["", "Cancelled", "Disputed", "Resolved", "Shipped"];

try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $orderDate = $_GET["orderDate"];
            if ($orderDate != '') {
                $orderDate = check_date($_GET["orderDate"]);
            }

            $requiredDate = $_GET["requiredDate"];
            if ($requiredDate != '') {
                $requiredDate = check_date($_GET["requiredDate"]);
            }

            $shippedDate = $_GET["shippedDate"];
            if ($shippedDate != '') {
                $shippedDate = check_date($_GET["shippedDate"]);
            }

            $status = $_GET["status"];
            if (!in_array($status, STATUS_WHITELIST)) {
                throw new Exception("Status is not valid", 232);
            }

            $comments = sanitize($_GET["comments"], PARANOID);

            $customerNumber = $_GET["customerNumber"];
            if ($customerNumber != '') {
                if (check_int($_GET["customerNumber"]))
                    $customerNumber = sanitize_int($_GET["customerNumber"]);
            }

            $result = $orders->getAll(array(
                "orderDate" => $orderDate,
                "requiredDate" => $requiredDate,
                "shippedDate" => $shippedDate,
                "status" => $status,
                "comments" => $comments,
                "customerNumber" => $customerNumber
            ));
            break;

        case "POST":
            $orderDate = $_POST["orderDate"];
            if ($orderDate != '') {
                $orderDate = check_date($_POST["orderDate"]);
            }

            $requiredDate = $_POST["requiredDate"];
            if ($requiredDate != '') {
                $requiredDate = check_date($_POST["requiredDate"]);
            }

            $shippedDate = $_POST["shippedDate"];
            if ($shippedDate != '') {
                $shippedDate = check_date($_POST["shippedDate"]);
            }

            $status = $_POST["status"];
            if (!in_array($status, STATUS_WHITELIST)) {
                throw new Exception("Status is not valid", 232);
            }

            $comments = sanitize($_POST["comments"], PARANOID);

            $customerNumber = $_POST["customerNumber"];
            if ($customerNumber != '') {
                if (check_int($_POST["customerNumber"]))
                    $customerNumber = sanitize_int($_POST["customerNumber"]);
            }

            $result = $orders->insert(array(
                "orderDate" => $orderDate,
                "requiredDate" => $requiredDate,
                "shippedDate" => $shippedDate,
                "status" => $status,
                "comments" => $comments,
                "customerNumber" => $customerNumber
            ));
            break;

        case "PUT":
            parse_str(file_get_contents("php://input"), $_PUT);
            $orderNumber = $_PUT["orderNumber"];
            if ($orderNumber != '') {
                if (check_int($_PUT["orderNumber"]))
                    $orderNumber = sanitize_int($_PUT["orderNumber"]);
            }

            $orderDate = $_PUT["orderDate"];
            if ($orderDate != '') {
                $orderDate = check_date($_PUT["orderDate"]);
            }

            $requiredDate = $_PUT["requiredDate"];
            if ($requiredDate != '') {
                $requiredDate = check_date($_PUT["requiredDate"]);
            }

            $shippedDate = $_PUT["shippedDate"];
            if ($shippedDate != '') {
                $shippedDate = check_date($_PUT["shippedDate"]);
            }

            $status = $_PUT["status"];
            if (!in_array($status, STATUS_WHITELIST)) {
                throw new Exception("Status is not valid", 232);
            }

            $comments = sanitize($_PUT["comments"], PARANOID);

            $customerNumber = $_PUT["customerNumber"];
            if ($customerNumber != '') {
                if (check_int($_PUT["customerNumber"]))
                    $customerNumber = sanitize_int($_PUT["customerNumber"]);
            }


            $result = $orders->update(array(
                "orderNumber" => intval($orderNumber),
                "orderDate" => $orderDate,
                "requiredDate" => $requiredDate,
                "shippedDate" => $shippedDate,
                "status" => $status,
                "comments" => $comments,
                "customerNumber" => $customerNumber
            ));
            break;

        case "DELETE":
            parse_str(file_get_contents("php://input"), $_DELETE);
            $orderNumber = $_DELETE["orderNumber"];
            if ($orderNumber != '') {
                if (check_int($_DELETE["orderNumber"]))
                    $orderNumber = sanitize_int($_DELETE["orderNumber"]);
            }

            $result = $orders->remove(intval($_DELETE["orderNumber"]));
            break;
    }


    header("Content-Type: application/json");
    echo json_encode($result);

}
catch (Exception $exception)
{
    echo json_encode(array(
        'error' => array(
            'msg' => $exception->getMessage(),
            'code' => $exception->getCode(),
        ),
    ));
}
