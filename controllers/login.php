<?

include "../functions/sanitize.php";
require_once "../functions/db.php";
require_once "../web/index.php";
require_once "../functions/template.php";
require_once '../functions/globals.php';


if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $login = sanitize($_POST["login"], PARANOID);
    $pwd = sanitize($_POST["password"], PARANOID);
    $password = hash("md5", $login . $pwd . 'salt');

    // Connect as admin to retrieve user info
    $db = connect("mysql:host=localhost;dbname=classicmodels", "admin", "admin");
    $sql = "SELECT users.user_id, users.access, users.banned FROM users WHERE ((users.login = :login) AND (users.password = :password))";
    $q = $db->prepare($sql);

    $q->bindParam(":login", $login);
    $q->bindParam(":password", $password);
    $q->execute();
    $res = $q->fetchAll();

    if (empty($res))
    {
        echo "NO SUCH USER FOUND";
        print_debug('There is no such user');
    }
    else
    {

        session_regenerate_id();
        $session_id = session_id();
        $user_id = $res[0]["user_id"];
        $access = $res[0]["access"];
        $banned = $res[0]["banned"];

        $idsql = "UPDATE users SET session_id = :session_id WHERE user_id = :user_id";
        $q = $db->prepare($idsql);
        $q->bindParam(":session_id", $session_id);
        $q->bindParam(":user_id", $user_id);
        $q->execute();


        if ($banned == 1)
        {
            echo "Begone. You've been banned. Shame on you.";
            $_SESSION["authorized"] = False;
            print_debug("Banned user tried to login");
            session_destroy();
            die();
        }
        print_debug('User logged in with access level of ' . $access);
        $_SESSION["access"] = $access;
        $_SESSION["login"] = $login;
        $_SESSION["password"] = $pwd;
        $_SESSION["session_id"] = $session_id;

        header("Location: /login");
    }

}

function logout() {
    $_SESSION["access"] = 3;
    $_SESSION["login"] = 3;
    $_SESSION["password"] = 3;
    $_SESSION["session_id"] = 0;
    session_destroy();
    header("Location: /");
    die();
}