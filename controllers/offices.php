<?php

include "../models/OfficeRepository.php";
include ("../config/config.php");
include "../functions/db.php";
$db = connect("mysql:host=localhost;dbname=classicmodels", "manager", "manager");
$countries = new OfficeRepository($db);

try {
    switch ($_SERVER["REQUEST_METHOD"]) {
        case "GET":
            $result = $countries->getAll(array());
            break;
    }


    header("Content-Type: application/json");
    echo json_encode($result);
}
catch (Exception $exception)
{
    echo json_encode(array(
        'error' => array(
            'msg' => $exception->getMessage(),
            'code' => $exception->getCode(),
        ),
    ));
}
