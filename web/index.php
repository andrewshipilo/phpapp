<?php
/*
    Проверять права пользователей на уровне роутинга.
    Проверять session_id.
*/

// Create the Home URL
$url_base = trim(str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME'])), '/');
$url_home = 'http://' . trim($_SERVER['HTTP_HOST'], '/') . (trim($url_base) !== "" ? '/' . $url_base : "");

// Load the Required Functions
require_once '../functions/route.php';
require_once '../functions/template.php';
require_once '../functions/db.php';

$error = false;
session_start();

if (!isset($_SESSION["access"]))
{
    $_SESSION["access"] = 3;
    $_SESSION["login"] = "guest";
    $_SESSION["password"] = "guest";
    $_SESSION["session_id"] = 0;
}

if ($_SESSION["access"] != 3)
{
    require_once '../controllers/login.php';
    $login = $_SESSION["login"];
    $pwd = $_SESSION["password"];
    $password = hash("md5", $login . $pwd . 'salt');

    $db = connect("mysql:host=localhost;dbname=classicmodels", "admin", "admin");
    $sql = "SELECT users.session_id FROM users WHERE ((users.login = :login) AND (users.password = :password))";
    $q = $db->prepare($sql);

    $q->bindParam(":login", $login);
    $q->bindParam(":password", $password);
    $q->execute();

    $res = $q->fetchAll();


    //Check session id
    if (session_id() != $res[0]["session_id"]) {
        logout();
    }
}
// Employees page => 'employees/'
route_add('employees/', function() use($url_home) {
    if ($_SESSION['access'] <= 2) {
        template_create('employees', array(
            'title' => 'Employees Database',
            'login_name' => $_SESSION['login']
        ));
    } else {
        header('Location: /error');
    }
});

route_add('employees/(:any)', function ($action = "") use($url_home) {
    if ($_SESSION['access'] <= 2) {
        require '../controllers/employees.php';
    } else {
        header('Location: /error');
    }
});

route_add('offices/(:any)', function ($action = "") use($url_home) {
    require '../controllers/offices.php';
});

route_add('orders/', function() use($url_home) {
    template_create('orders', array(
        'title' => 'Orders Database',
        'login_name' => $_SESSION['login']
    ));
});

route_add('orders/(:any)', function ($action = "") use($url_home) {
    require '../controllers/orders.php';
});

route_add('customers/(:any)', function ($action = "") use($url_home) {
    require '../controllers/customers.php';
});

// Login => 'login/'
route_add('login/', function() use($url_home) {
    template_create('login', array(
        'title' => 'Login',
        'login_name' => $_SESSION['login']
    ));
});
// Login => 'login/?'
route_add('login/(:any)', function() use($url_home) {
    require '../controllers/login.php';
});

// Logout => 'logout/'
route_add('logout/', function() use($url_home) {
    require_once '../controllers/login.php';
    logout();
});

// Users => 'users/'
route_add('/users', function () use ($url_home) {
    if ($_SESSION['access'] == 1) {
        template_create('users', array(
            'title' => 'Users',
            'login_name' => $_SESSION['login']
        ));
    } else {

        header('Location: /error');
    }
});

// Users => 'users/'
route_add('/users/(:any)', function () use ($url_home) {
    if ($_SESSION['access'] == 1) {
        require '../controllers/users.php';
    }
    else {
        header('Location: /error');
    }
});


// Home Page => `/`
route_add('/', function() use($url_home) {
    template_create('home', array(
        'title' => 'Home Page',
        'login_name' => $_SESSION['login']
    ));
});

route_add('error/', function () use($url_home)
{
    template_create('error', array(
        'title' => 'Users',
        'login_name' => $_SESSION['login'],
        'error' => '404. Route not found'
    ));
});

// Do Routing
route_execute();

#template_create('error', array(
#    'title' => 'Error'
#));