/*global $ */

$(function () {

    $("#jsGrid").jsGrid({
        height: "500px",
        width: "100%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 5,
        pageButtonCount: 5,
        deleteConfirm: "Do you really want to delete order?",
        controller: {
            loadData: function (filter) {
                return $.ajax({
                    type: "GET",
                    url: "/users/get",
                    data: filter,
                    success: function(data) {
                        if (data.error) {
                            // handle the error
                            alert(data.error.msg);
                            //throw data.error.msg;
                        }
                    }
                }).done(function (data) {
                    if (data.error) {
                        // handle the error
                        alert(data.error.msg);
                        //throw data.error.msg;
                    }
                });
            },
            insertItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "/users/insert",
                    data: item,
                    success: function(data) {
                        if (data.error) {
                            // handle the error
                            alert(data.error.msg);
                            //throw data.error.msg;
                        }
                    }
                });
            },
            updateItem: function (item) {
                return $.ajax({
                    type: "PUT",
                    url: "/users/update",
                    data: item,
                    success: function(data) {
                        if (data.error) {
                            // handle the error
                            alert(data.error.msg);
                            //throw data.error.msg;
                        }
                    }
                });
            },
            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/users/delete",
                    data: item,
                    success: function(data) {
                        if (data.error) {
                            // handle the error
                            alert(data.error.msg);
                            //throw data.error.msg;
                        }
                    }
                });
            }
        },
        fields: [
            {name: "login", title: "Login", type: "text", width: 150},
            {name: "password", title: "Password", type: "text", width: 150},
            {name: "access", title: "Access", type: "text", width: 150},
            {name: "banned", title: "Banned", type: "text", width: 150},
            {name: "session_id", title: "Session ID", type: "text", width: 150},
            {type: "control"}
        ]
    });
});
