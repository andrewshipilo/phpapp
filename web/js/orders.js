/*global $ */

$(function () {

    var MyDateField = function (config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function (date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function (value) {
            return new Date(value).toDateString();
        },

        insertTemplate: function (value) {
            return this._insertPicker = $("<input>").datepicker({defaultDate: new Date()});
        },

        editTemplate: function (value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },

        insertValue: function () {
            return this._insertPicker.datepicker("getDate").toISOString();
        },

        editValue: function () {
            return this._editPicker.datepicker("getDate").toISOString();
        }
    });

    jsGrid.fields.datePicker = MyDateField;
    $.ajax({
        type: "GET",
        url: "/customers/get"
    }).done(function(customers) {
        $("#jsGrid").jsGrid({
            height: "500px",
            width: "100%",
            filtering: true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 5,
            pageButtonCount: 5,
            deleteConfirm: "Do you really want to delete order?",
            controller: {
                loadData: function (filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/orders/get",
                        data: filter,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    }).done(function (data) {
                        if (data.error) {
                            // handle the error
                            alert(data.error.msg);
                            //throw data.error.msg;
                        }
                    });
                },
                insertItem: function (item) {
                    return $.ajax({
                        type: "POST",
                        url: "/orders/insert",
                        data: item,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                },
                updateItem: function (item) {
                    return $.ajax({
                        type: "PUT",
                        url: "/orders/update",
                        data: item,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                },
                deleteItem: function (item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/orders/delete",
                        data: item,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                }
            },
            fields: [
                {name: "orderDate", title: "Order Date", type: "datePicker", width: 75},
                {name: "requiredDate", title: "Required Date", type: "datePicker", width: 75},
                {name: "shippedDate", title: "Shipped Date", type: "datePicker", width: 75},
                {name: "status", title: "Status", type: "text", width: 150},
                {name: "comments", title: "Comments", type: "text", width: 150},
                {name: "customerNumber", title: "Customer", type: "text", width: 150},
                {type: "control"}
            ]
        });
    });
});
