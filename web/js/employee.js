/*global $ */

$(function () {

    var MyDateField = function (config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function (date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function (value) {
            return new Date(value).toDateString();
        },

        insertTemplate: function (value) {
            return this._insertPicker = $("<input>").datepicker({defaultDate: new Date()});
        },

        editTemplate: function (value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },

        insertValue: function () {
            return this._insertPicker.datepicker("getDate").toISOString();
        },

        editValue: function () {
            return this._editPicker.datepicker("getDate").toISOString();
        }
    });

    jsGrid.fields.datePicker = MyDateField;

    $.ajax({
        type: "GET",
        url: "/offices/get"
    }).done(function(offices) {
        $("#jsGrid").jsGrid({
            height: "500px",
            width: "100%",
            filtering: true,
            inserting: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
            pageSize: 10,
            pageButtonCount: 5,
            deleteConfirm: "Do you really want to delete client?",
            controller: {
                loadData: function (filter) {
                    return $.ajax({
                        type: "GET",
                        url: "/employees/get",
                        data: filter,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                },
                insertItem: function (item) {
                    return $.ajax({
                        type: "POST",
                        url: "/employees/insert",
                        data: item,
                        success: function(data) {
                            console.log('OmegaLUL');
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                },
                updateItem: function (item) {
                    return $.ajax({
                        type: "PUT",
                        url: "/employees/update",
                        data: item,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                },
                deleteItem: function (item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/employees/delete",
                        data: item,
                        success: function(data) {
                            if (data.error) {
                                // handle the error
                                alert(data.error.msg);
                                //throw data.error.msg;
                            }
                        }
                    });
                }
            },
            fields: [
                {name: "lastName", title: "Last Name", type: "text", width: 75},
                {name: "firstName", title: "Name", type: "text", width: 75},
                {name: "extension", title: "Ext", type: "text", width: 75},
                {name: "email", title: "Email", type: "text", width: 150},
                {name: "officeCode", title: "Office", type: "select", width: 150, items: offices, valueField: "officeCode", textField: "city" },
                {
                    name: "reportsTo",
                    title: "Reports To",
                    type: "text",
                    width: 50,

                },
                {name: "jobTitle", title: "Title", type: "text", width: 150},
                {type: "control"}
            ]
        });
    });
});
