<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once "../functions/debug.php";
define('DEBUG', true);

define('LOGFILE', __DIR__ . '/../logs/error.log');
define('DEBUGLOG', __DIR__ . '/../logs/debug.log');
define('PRODLOG', __DIR__ . '/../logs/prod.log');


if (defined('DEBUG'))
{
    ini_set('error_log', __DIR__ . '/../logs/error.log');
    if (DEBUG === true)
    {
        error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        ini_set('html_errors', 1);
    } else if (DEBUG === false)
    {
        error_reporting(E_NOTICE);
        ini_set('display_errors', false);
        ini_set('display_startup_errors', false);
    }
}

date_default_timezone_set("Europe/Moscow");

mb_internal_encoding("UTF-8");

$DOMAIN = "http://localhost";


$config = array(
    "db" => "mysql:host=localhost;dbname=classicmodels",
    "username" => "manager",
    "password" => "123456",
    'driver'   => 'pdo_mysql',
    'charset'  => 'utf8'
);

$route_files = '../include/';
$route_templates = __DIR__.'/templates/';
